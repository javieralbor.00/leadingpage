import React from "react"
import { Link } from "gatsby"
import Grid from "@material-ui/core/Grid"
import Image from "../components/image"
import SEO from "../components/seo"
import Img from "gatsby-image"
import "./index.css"
import ScrollAnim from "rc-scroll-anim"

const Element = ScrollAnim.Element

const IndexPage = () => (
  <Grid>
    <SEO title="Home" />
    <Element className="pack-page page0" id="page0">
      <Grid item className="nav">
        <p className="text">
          En este momento de contingencia sabemos por lo que tu negocio está
          pasando y queremos ayudarte, ofrecemos hasta 73% de descuento
        </p>
      </Grid>
    </Element>
    <Element>
      <Grid
        style={{
          maxWidth: `300px`,
          marginBottom: `1.45rem`,
        }}
      >
        <Image />
      </Grid>
    </Element>
  </Grid>
)

export default IndexPage
